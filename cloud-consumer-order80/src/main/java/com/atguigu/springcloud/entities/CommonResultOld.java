package com.atguigu.springcloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
// @NoArgsConstructor
public class CommonResultOld<T> {

    private Integer code;
    private String message;
    private T data;

    public CommonResultOld(Integer code, String message) {
        this(code, message, null);
    }

}
