package com.atguigu.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MySelfRule {

    @Bean
    public IRule myRule() {
        // Ribbon负载规则定义为随机
        // 不能和ComponentScan放置在同包或者子包下面，否则修改的负载规则会应用到其他地方，非定制化修改了
        return new RandomRule();
    }

}
